# How to resize fonts
## https://github.com/Hypnootize/hypnotize-hud/issues/26
If you want to resize your fonts manually in the future, download fontforge, which should be available for most major distributions. Once you have opened up the font file in font forge, Ctrl + A all of the characters, click the Element tab, and Transform. Change from Move, to Scale Uniformly, and scale down to 75%. Then, click on File, and Generate Fonts. Make sure you have selected the font file format same as the original, and same name.
