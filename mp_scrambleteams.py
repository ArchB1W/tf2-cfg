#!/usr/bin/env python3

import random


def even(num: float) -> bool:
    return num % 2 == 0


def odd(num: float) -> bool:
    return num % 2 != 0


def randomizeTeams(
    players: set[str], teamNames: tuple[str, str]
) -> dict[str, list[str]]:
    playersAvailable = players.copy()
    teams: dict[str, list[str]] = {teamName: [] for teamName in teamNames}
    playerPoolEven = even(len(players))
    for index in range(1, len(players) + 1):
        # Pick player
        choice = random.choice(tuple(playersAvailable))
        playersAvailable.remove(choice)
        # If there's only one player left to pick and the player pool is uneven
        # then it will randomly pick a team to get the extra player.
        # This is different to the built-in command which always picks BLU
        if len(playersAvailable) == 0 and not playerPoolEven:
            randomTeam = random.choice(teamNames)
            teams[randomTeam].append(choice)
        else:
            if odd(index):
                teams[teamNames[0]].append(choice)
            else:
                teams[teamNames[1]].append(choice)

    return teams


def main() -> None:
    players = set(
        filter(
            lambda s: not s.startswith("#"),
            map(lambda s: s.strip(), open("scrambleplayers.txt").readlines()),
        )
    )
    teamNames = ("BLU", "RED")

    teams = randomizeTeams(players, teamNames)
    for team in teams:
        print(f"{team}:\n", "\t" + "\n\t".join(teams[team]))


main()
