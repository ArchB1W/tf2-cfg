#!/usr/bin/env python3


import random


maps = {
    "pl_upward",
    "pl_badwater",
    "pl_pier",
    "pl_swiftwater"
    "koth_viaduct",
    "koth_brazil",
}


mapOptions = maps.copy()
# Get random map from unrankedMaps
for index, map in enumerate(maps):
    randomMap = random.choice(tuple(mapOptions))
    mapOptions.remove(randomMap)
    print(f"Choice {index+1}: {randomMap}")
